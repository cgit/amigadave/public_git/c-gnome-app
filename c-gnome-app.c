#include <gtk/gtk.h>

static GtkWidget *window;

static void
on_hello_world (GSimpleAction *action,
                GVariant *parameter,
                gpointer user_data)
{
    g_print ("%s\n", "Hello world!");
}

static void
on_quit (GSimpleAction *action,
         GVariant *parameter,
         gpointer user_data)
{
    g_application_quit (G_APPLICATION (user_data));
}

static const GActionEntry actions[] =
{
    { "hello-world", on_hello_world },
    { "quit", on_quit }
};

static void
on_activate (GApplication *app,
             gpointer user_data)
{
    gtk_widget_show_all (window);
}

static void
on_startup (GApplication *app,
            gpointer user_data)
{
    GtkApplication *gtk_app;
    GtkWidget *button;
    const gchar * const hello_world_accel[] = { "<Primary>h", NULL };
    const gchar * const quit_accel[] = { "<Primary>q", NULL };
    GMenu *appmenu;
    GMenu *section;
    GMenuItem *item;

    gtk_app = GTK_APPLICATION (app);
    g_action_map_add_action_entries (G_ACTION_MAP (app), actions,
                                     G_N_ELEMENTS (actions), app);
    window = gtk_application_window_new (gtk_app);

    button = gtk_button_new_with_label ("Hello world!");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (button), "app.hello-world");
    gtk_container_add (GTK_CONTAINER (window), button);

    gtk_application_set_accels_for_action (gtk_app,
                                           "app.hello-world",
                                           hello_world_accel);
    gtk_application_set_accels_for_action (gtk_app,
                                           "app.quit",
                                           quit_accel);

    appmenu = g_menu_new ();
    section = g_menu_new ();
    item = g_menu_item_new ("Hello world!", "app.hello-world");
    g_menu_append_section (appmenu, NULL, G_MENU_MODEL (section));
    g_menu_append_item (section, item);
    g_object_unref (item);
    item = g_menu_item_new ("Quit", "app.quit");
    g_menu_append_item (section, item);
    g_object_unref (item);
    gtk_application_set_app_menu (gtk_app, G_MENU_MODEL (appmenu));

    g_object_unref (appmenu);
}

int
main (int argc,
      char *argv[])
{
    GtkApplication *app;
    gint status;

    app = gtk_application_new ("org.example.CGnome", G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);
    g_signal_connect (app, "startup", G_CALLBACK (on_startup), NULL);
    status = g_application_run (G_APPLICATION (app), argc, argv);

    g_object_unref (app);

    return status;
}
